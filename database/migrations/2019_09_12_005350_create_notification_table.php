<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('id_notification');
            $table->string('description',150)->nullable(true);
            $table->string('viewed',150)->nullable(true);
            $table->integer('id_account')->nullable(true)->unsigned();
            $table->integer('id_room')->nullable(true)->unsigned();
            $table->integer('id_type_notification')->nullable(true)->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
