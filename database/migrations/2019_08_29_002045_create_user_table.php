<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id_user');
            $table->string('first_name',150)->nullable(true);
            $table->string('last_name',200)->nullable(true);
            $table->string('email_user',150)->nullable(true);
            $table->string('phone_user',150)->nullable(true);
            $table->string('address_user',255)->nullable(true);
            $table->string('zip_code',50)->nullable(true);
            $table->string('username',150)->nullable(true);
            $table->string('password',255)->nullable(true);
            $table->boolean('active_user')->nullable(true);
            $table->integer('id_type_role')->nullable(true)->unsigned();
            $table->integer('id_account')->nullable(true)->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
