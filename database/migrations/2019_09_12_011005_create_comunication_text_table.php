<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicationTextTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunication_text', function (Blueprint $table) {
            $table->increments('id_communication_text');
            $table->text('body_text')->nullable(true);
            $table->string('phone_client',150)->nullable(true);
            $table->date('date_text')->nullable(true);
            $table->boolean('status_text')->nullable(true);
            $table->integer('type_direction')->nullable(true);
            $table->integer('id_room')->nullable(true)->unsigned();
            $table->integer('id_account')->nullable(true)->unsigned();
            $table->bigInteger('id_user')->nullable(true)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunication_text');
    }
}
