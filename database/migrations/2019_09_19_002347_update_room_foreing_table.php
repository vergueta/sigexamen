<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateRoomForeingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('room', function (Blueprint $table) {
            $table->foreign('id_account')->references('id_account')->on('account');
            $table->foreign('id_state')->references('id_state')->on('state');
            $table->foreign('id_country')->references('id_country')->on('country');
            $table->foreign('id_reputation')->references('id_reputation')->on('reputation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('room', function (Blueprint $table) {
            $table->dropForeign(['id_account']);
            $table->dropForeign(['id_state']);
            $table->dropForeign(['id_country']);
            $table->dropForeign(['id_reputation']);
        });
    }
}
